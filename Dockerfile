FROM php:apache

RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN apt-get install -y libc-bin
RUN apt-get update && apt-get install -y libpng-dev libjpeg-dev libpq-dev libfreetype6-dev libzip-dev
RUN apt-get install -y libblkid1

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /var/www/html

COPY src .

EXPOSE 80
CMD ["apache2-foreground"]