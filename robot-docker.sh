#!/bin/bash
docker run -it \
-v $(pwd)/reports:/opt/robotframework/reports:Z \
-v $(pwd)/robotframework/:/opt/robotframework/tests:Z \
registry.gitlab.com/docker42/rfind:master \
/opt/robotframework/bin/run-tests-in-virtual-screen $PABOT_OPT $ROBOT_OPT -v BROWSER:firefox /opt/robotframework/tests/$PROJECT
