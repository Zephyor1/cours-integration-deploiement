<?php

class City
{
    private $cities = [
        1 => 'Bordeaux',
        2 => 'Paris',
        3 => 'Marseille',
        // Add more cities as needed
    ];

    public function getCityNameById($id)
    {
        return $this->cities[$id] ?? null;
    }

    public function addCity($id, $name)
    {
        $this->cities[$id] = $name;
    }

    public function getAllCities()
    {
        return $this->cities;
    }

    public function removeCity($id)
    {
        unset($this->cities[$id]);
    }
}

