<?php

require_once './src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once('./src/City.php');

class CityTest extends \PHPUnit\Framework\TestCase
{
    public function testGetCityNameById()
    {
        $city = new City();
        $result = $city->getCityNameById(1);
        $expected = 'Bordeaux';
        $this->assertEquals($expected, $result);
    }

    public function testGetCityNameByIdForNonExistingCity()
    {
        $city = new City();
        $result = $city->getCityNameById(999);
        $this->assertNull($result);
    }

    public function testAddCity()
    {
        $city = new City();
        $city->addCity(2, 'Paris');
        $result = $city->getCityNameById(2);
        $expected = 'Paris';
        $this->assertEquals($expected, $result);
    }

    public function testGetAllCities()
    {
        $city = new City();
        $result = $city->getAllCities();
        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
        $this->assertArrayHasKey(1, $result);
        $this->assertArrayHasKey(2, $result);
        // Add more specific assertions based on your implementation
    }

    public function testRemoveCity()
    {
        $city = new City();
        $city->removeCity(1);
        $result = $city->getCityNameById(1);
        $this->assertNull($result);
    }

    // Add more test methods as needed
}
